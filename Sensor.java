/**
 * En abstraksjon av en fysisk sensor. Er ansvarlig for kommunikasjon
 * med en eventuell fysisk sensor; i vårt tilfelle gjør vi istedet
 * tilgjengelig en metode <code>setSmokeDetected</code> for testing.
 */
public class Sensor {
    boolean smokeDetected;
    FireProtectionSystem system;
    String ip;

    Sensor(String ip) {
        this.ip = ip;
    }

    /**
     * Setter hvorvidt røyk er oppdaget.
     * @param smokeDetected
     */
    public void setSmokeDetected(boolean smokeDetected) {
        this.smokeDetected = smokeDetected;
        if (smokeDetected) {
            system.smokeDetected();
        }
        else {
            system.stoppedDetectingSmoke();
        }
    }

    /**
     * Installer et brannvarslingssystem denne sensoren rapporterer til
     * når den oppdager røyk.
     * @param system
     */
    void installSystem(FireProtectionSystem system) {
        this.system = system;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Sensor)) {
            return false;
        }
        Sensor that = (Sensor) obj;
        return this.ip.equals(that.ip);
    }

    @Override
    public int hashCode() {
        return 1;
    }

}
